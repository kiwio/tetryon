# Tetryon #

Tetryon is a high performance concurrency management framework. It's purpose is to pass events from thread to another (possibly many) in the smallest time possible.

## Features ##
 - Using Tetryon, benchmarks have shown it's possible to pass up to 240 million events a second on only two cores. Ideal for high throughput environments.
 - Events are guaranteed to be processed in order they were published.
 - Multi publisher feature available if one wishes to publish events from multiple threads
 - Multiple consumers can be added, each will be given their own thread. Each consumer will still handle every single event that's published.
 - Garbage free 
 - Bounded queue to prevent blowing up the heap.
 
## Multi publisher + Multi consumer example ##
This example requires you to provide some more of the event handling information.

#### Arguments ####
 - Capacity - This is how many event instances the queue will hold in it's buffer.
 - NewEventSupplier - This tells Tetryon how to build a new blank event, this is only used once at construction. The reference to the event is immutable so it's fields should not be.
 - EventOverwriter - This tells Tetryon how to replace the event in the queue, this should replace all the fields in the event
 - List of consumer handlers - These are the consumers that will handle each event. Each consumer will handle every single event that is published in order.

```java
public class MultiConsumerExample
{
    public static void main(String[] args) {
        final Cat cat = new Cat();
        final Cat cat2 = new Cat();
        final Tetryon<CatEvent> catTetryon = Tetryon.buildInstance(1024, () -> new CatEvent(NONE), (instanceToReplace, replacementInstance) -> {
            instanceToReplace.overwrite(replacementInstance);
            return instanceToReplace;
        }, Arrays.asList(cat::doThing, cat2::doThing), 100);

        catTetryon.start();
        catTetryon.publishEvent(new CatEvent(EAT));
        catTetryon.publishEvent(new CatEvent(NAP));
    }
    

    public static class Cat {
        private long ateFoodCount = 0;
        private long napCount = 0;


        public void doThing(CatEvent event) {
            switch (event.catAction) {

                case NONE:
                    break;
                case EAT:
                    ateFoodCount++;
                    break;
                case NAP:
                    napCount++;
                    break;
            }
        }
    }

    public static class CatEvent {
        private CatAction catAction = NONE;

        public CatEvent(CatAction catAction) {

            this.catAction = catAction;
        }

        public void overwrite(CatEvent replacementInstance) {
            this.catAction = replacementInstance.catAction;
        }

        public enum CatAction {
            NONE,
            EAT,
            NAP
        }
    }
}
```

## Performance results ##

Run on an Intel Core i7-6700K CPU 4.00GHz. Each of these benchmarks are testing raw throughput of multiple implementations of Tetryon and other comparable frameworks. 
They each simple publish an event which will eventually reach a consumer that does nothing. Measuring only how many times a second it can get an event through the queue.

```
    ExecutorBenchmark.throughExecutorWithArrayBlockingList         thrpt  100    9486859.498 ±  404947.573  ops/s
    DisruptorBlockWaitBenchmark.throughDisruptorBlocking           thrpt  100   19312876.142 ± 2784608.470  ops/s
    TetryonMultiBenchmark.throughTetryonMultiPublisherMode         thrpt  100   48671136.503 ± 1563759.683  ops/s
    DisruptorYieldingBenchmark.throughDisruptorYielding            thrpt  100   99040036.423 ±  832909.595  ops/s
    TetryonMultiConsumerBenchmark.throughTetryonMultipleConsumers  thrpt  100  118652027.974 ± 7365225.376  ops/s
    EndToEndTetryonBenchmark.publishEndToEnd                       thrpt  100  247400171.144 ± 4345277.31   ops/s
```