package io.kiw.tetryon.index;

import io.kiw.tetryon.ExceptionHandler;
import io.kiw.tetryon.Tetryon;
import io.kiw.tetryon.unsafe.PartTimeVolatileLong;

public class ReadIndexHandler<T> {
    private final Tetryon.EventHandler<T> handler;
    private final PartTimeVolatileLong readIndex;
    private final ExceptionHandler exceptionHandler;
    private long writeIndexCache = 0L;

    public ReadIndexHandler(Tetryon.EventHandler<T> handler, PartTimeVolatileLong readIndex, ExceptionHandler exceptionHandler) {
        this.handler = handler;
         this.readIndex = readIndex;
         this.exceptionHandler = exceptionHandler;
     }

    public long getCurrentIndex() {
        return readIndex.getLong();
    }

    public void handleInstanceAndUpdateIndex(final T[] instances, final int limit, long currentReadIndex) {
        try
        {
            handler.handle(instances[(int)currentReadIndex++ & limit]);
        }
        catch (Throwable t)
        {
            exceptionHandler.handle(t);
        }
        finally {
            setIndex(currentReadIndex);
        }
    }

    private void setIndex(long index) {
        this.readIndex.putOrderedLong(index);
    }

    public boolean queueIsEmpty(long currentReadIndex, PartTimeVolatileLong writeIndex) {
         if(currentReadIndex == writeIndexCache) // local cache believes we are empty
         {
             writeIndexCache = writeIndex.getVolatileLong(); // check main memory
             return currentReadIndex == writeIndexCache;
         }

         return false;
    }


}
