package io.kiw.tetryon.index;

import io.kiw.tetryon.unsafe.PartTimeVolatileLong;

public class ReadIndexesTracker {
    private final PartTimeVolatileLong[] readIndexes;

    public ReadIndexesTracker(PartTimeVolatileLong[] readIndexes) {

        this.readIndexes = readIndexes;
    }

    public long getLowestReadIndexSafely() {
        long lowestSeen = Long.MAX_VALUE;
        for (PartTimeVolatileLong readIndex : readIndexes) {
            lowestSeen = Math.min(readIndex.getVolatileLong(), lowestSeen);
        }

        return lowestSeen;
    }
}
