package io.kiw.tetryon;

public interface TetryonPublisherBatcher<T> {
    void startBatch();

    void publishEvent(T overwrite);

    void flushBatch();
}
