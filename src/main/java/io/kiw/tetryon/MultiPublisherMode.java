package io.kiw.tetryon;

import io.kiw.tetryon.publishermode.*;

import java.util.*;

public class MultiPublisherMode<T> implements PublisherMode<T> {

    private final int publisherThreadCount;

    public MultiPublisherMode(int publisherThreadCount) {
        this.publisherThreadCount = publisherThreadCount;
    }

    @Override
    public void start()
    {

    }

       @Override
    public PublisherModeHandler<T> buildPublisherModeHandler(List<Tetryon.EventHandler<T>> eventHandlers, EventOverwriter<T> eventOverwriter, long readParkTimeNanos, int capacity, NewEventSupplier<T> newEventSupplier, ExceptionHandler exceptionHandler) {

       PublisherModeHandler<T>[] singlePublisherPerThread = new SinglePublisherModeHandler[publisherThreadCount];
        for (int i = 0; i < publisherThreadCount; i++) {
            singlePublisherPerThread[i] = new SinglePublisherMode<T>().buildPublisherModeHandler(eventHandlers, eventOverwriter, readParkTimeNanos, capacity, newEventSupplier, exceptionHandler);
        }

        return new MultiPublisherModeHandler<>(singlePublisherPerThread);
    }
}
