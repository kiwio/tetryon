package io.kiw.tetryon;

public interface NewEventSupplier<T> {
    T newInstance();
}
