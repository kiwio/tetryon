package io.kiw.tetryon;

import io.kiw.tetryon.publishermode.PublisherModeHandler;

import java.util.List;
import java.util.concurrent.ThreadFactory;

public class Tetryon<T> {
    private final Thread[] consumerThreads;
    private final PublisherModeHandler<T> publisherModeHandler;

    private Tetryon(PublisherModeHandler<T> publisherModeHandler, Thread[] consumerThreads) {
        this.publisherModeHandler = publisherModeHandler;
        this.consumerThreads = consumerThreads;
    }

    public void start() {
        for (Thread consumerThread : consumerThreads) {
            consumerThread.start();
        }
    }

    public void publishEvent(final T t) {
        publisherModeHandler.publishEvent(t, 0);
    }

    public void publishEvent(final T t, int publisherThreadIndex) {
        publisherModeHandler.publishEvent(t, publisherThreadIndex);
    }

    public void await()
    {
        publisherModeHandler.await();
    }

    public void shutdown()
    {
        await();
        for (Thread consumerThread : consumerThreads) {
            consumerThread.interrupt();
        }

    }

    public TetryonPublisherBatcher<T> getBatcher() {
        return publisherModeHandler.getBatcher();
    }

    public interface EventHandler<M> {

        void handle(M instruction);
    }

    public static <T> Tetryon<T> buildInstance(
            int capacity,
            NewEventSupplier<T> newEventSupplier,
            EventOverwriter<T> eventOverwriter,
            List<EventHandler<T>> handlers,
            long readParkTimeNanos
    ) {
        ;
        return buildInstance(capacity, newEventSupplier, eventOverwriter, new LazyMultiPublisherMode<>(), handlers, readParkTimeNanos, (t) -> {}, new TetryonThreadFactory());
    }

    public static <T> Tetryon<T> buildInstance(
            int capacity,
            NewEventSupplier<T> newEventSupplier,
            EventOverwriter<T> eventOverwriter,
            PublisherMode<T> publisherMode,
            List<EventHandler<T>> handlers,
            long readParkTimeNanos,
            ExceptionHandler exceptionHandler, ThreadFactory threadFactory
    ) {

        PublisherModeHandler<T> publisherModeHandler = publisherMode.buildPublisherModeHandler(handlers, eventOverwriter, readParkTimeNanos, capacity, newEventSupplier, exceptionHandler);
        Thread[] consumerThreads = buildConsumerThreads(publisherModeHandler, handlers, threadFactory);

        return new Tetryon<>(publisherModeHandler, consumerThreads);
    }

    private static <V> Thread[] buildConsumerThreads(PublisherModeHandler<V> publisherModeHandler, List<EventHandler<V>> consumerHandlers, ThreadFactory threadFactory) {
        TetryonComponents<V>[] tetryonComponents = publisherModeHandler.getTetryonComponents();
        Thread[] threads = new Thread[consumerHandlers.size()];
        for (int consumerIndex = 0; consumerIndex < consumerHandlers.size(); consumerIndex++)
        {
            threads[consumerIndex] = threadFactory.newThread(new TetryonConsumerFanOut<>(tetryonComponents, consumerIndex));
        }

        return threads;
    }
}
