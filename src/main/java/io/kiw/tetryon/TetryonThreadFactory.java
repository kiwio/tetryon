package io.kiw.tetryon;

import java.util.concurrent.ThreadFactory;

public class TetryonThreadFactory implements ThreadFactory {
    int index = 0;

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(r, "Tetryon-Consumer-" + index++);
    }
}
