package io.kiw.tetryon;

import io.kiw.tetryon.index.ReadIndexesTracker;
import io.kiw.tetryon.publishermode.PublisherModeHandler;

import java.util.List;

public interface PublisherMode<T> {

    void start();

    PublisherModeHandler<T> buildPublisherModeHandler(List<Tetryon.EventHandler<T>> handlers, EventOverwriter<T> eventOverwriter, long readParkTimeNanos, int capacity, NewEventSupplier<T> newEventSupplier, ExceptionHandler exceptionHandler);
}
