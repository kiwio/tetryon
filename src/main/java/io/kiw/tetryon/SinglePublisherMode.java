package io.kiw.tetryon;

import io.kiw.tetryon.publishermode.PublisherModeHandler;
import io.kiw.tetryon.publishermode.SinglePublisherModeHandler;

import java.util.List;

public class SinglePublisherMode<T> implements PublisherMode<T> {

    public static final int MAX_BATCH_SIZE = 128;

    @Override
    public void start() {

    }

    @Override
    public PublisherModeHandler<T> buildPublisherModeHandler(List<Tetryon.EventHandler<T>> eventHandlers, EventOverwriter<T> eventOverwriter, long readParkTimeNanos, int capacity, NewEventSupplier<T> newEventSupplier, ExceptionHandler exceptionHandler) {
        TetryonComponents<T> components = TetryonComponents.build(eventHandlers, eventOverwriter, readParkTimeNanos, capacity, newEventSupplier, exceptionHandler);

        T[] batchedItems = (T[])new Object[MAX_BATCH_SIZE];

        for (int i = 0; i < 128; i++) {
            batchedItems[i] = components.newEventSupplier.newInstance();
        }
        return new SinglePublisherModeHandler<>(components, capacity, batchedItems);
    }
}
