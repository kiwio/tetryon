package io.kiw.tetryon;

import io.kiw.tetryon.publishermode.PublisherModeHandler;

public class SingleTetryonPublisherBatcher<T> implements TetryonPublisherBatcher<T> {
    private PublisherModeHandler<T> publisherModeHandler;
    private final T[] batchedItems;
    private final EventOverwriter<T> eventOverwriter;
    int batchIndex = 0;

    public SingleTetryonPublisherBatcher(PublisherModeHandler<T> publisherModeHandler, T[] batchedItems, EventOverwriter<T> eventOverwriter) {

        this.publisherModeHandler = publisherModeHandler;
        this.batchedItems = batchedItems;
        this.eventOverwriter = eventOverwriter;
    }

    @Override
    public void startBatch() {
        batchIndex = 0;
    }

    @Override
    public void publishEvent(T overwrite) {
        eventOverwriter.overwrite(batchedItems[batchIndex++], overwrite);
    }

    @Override
    public void flushBatch() {
        publisherModeHandler.flushBatch(batchedItems, batchIndex);
        batchIndex = 0;
    }
}
