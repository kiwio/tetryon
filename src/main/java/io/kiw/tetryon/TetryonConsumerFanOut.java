package io.kiw.tetryon;

import static java.util.concurrent.locks.LockSupport.parkNanos;

class TetryonConsumerFanOut<T>  implements Runnable {


    private final TetryonComponents<T>[] allPublisherComponents;
    private final int consumerIndex;

    TetryonConsumerFanOut(TetryonComponents<T>[] allPublisherComponents, int consumerIndex) {
        this.allPublisherComponents = allPublisherComponents;
        this.consumerIndex = consumerIndex;
    }

    @Override
    public void run() {
        try {
            while (true) {
                boolean handledAnEvent = false;

                for (int publisherIndex = 0; publisherIndex < allPublisherComponents.length; publisherIndex++) {
                    TetryonComponents<T> publisherComponents = allPublisherComponents[publisherIndex];
                    handledAnEvent |= publisherComponents.tetryonQueue.handleEvent(publisherComponents.readIndexHandlers[consumerIndex]);
                }

                if(!handledAnEvent)
                {
                    parkNanos(100);
                    if(Thread.interrupted())
                    {
                        throw new InterruptedException("Tetryon Consumer Interrupted by another thread");
                    }
                }
            }
        } catch (InterruptedException ignore) {

        }
    }
}
