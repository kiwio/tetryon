package io.kiw.tetryon;

public interface ExceptionHandler {
    void handle(Throwable t);
}
