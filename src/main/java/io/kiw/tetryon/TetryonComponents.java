package io.kiw.tetryon;

import io.kiw.tetryon.index.ReadIndexHandler;
import io.kiw.tetryon.index.ReadIndexesTracker;
import io.kiw.tetryon.unsafe.PartTimeVolatileLong;

import java.util.ArrayList;
import java.util.List;

public class TetryonComponents<T> {

    public final ReadIndexHandler<T>[] readIndexHandlers;
    public final List<PartTimeVolatileLong> readIndexes;
    public final TetryonQueue<T> tetryonQueue;
    public final ReadIndexesTracker readIndexesTracker;
    public final NewEventSupplier<T> newEventSupplier;
    public final EventOverwriter<T> eventOverwriter;

    private TetryonComponents(ReadIndexHandler<T>[] readIndexHandlers, List<PartTimeVolatileLong> readIndexes, TetryonQueue<T> tetryonQueue, ReadIndexesTracker readIndexesTracker, NewEventSupplier<T> newEventSupplier, EventOverwriter<T> eventOverwriter) {
        this.readIndexHandlers = readIndexHandlers;
        this.readIndexes = readIndexes;
        this.tetryonQueue = tetryonQueue;
        this.readIndexesTracker = readIndexesTracker;
        this.newEventSupplier = newEventSupplier;
        this.eventOverwriter = eventOverwriter;
    }

    public static <T> TetryonComponents<T> build(List<Tetryon.EventHandler<T>> handlers, EventOverwriter<T> eventOverwriter, long readParkTimeNanos, int capacity, NewEventSupplier<T> newEventSupplier, ExceptionHandler exceptionHandler) {
        List<ReadIndexHandler<T>> readIndexHandlers = new ArrayList<>();
        List<PartTimeVolatileLong> readIndexes = new ArrayList<>();
        for (Tetryon.EventHandler<T> handler : handlers) {
            final PartTimeVolatileLong readIndex = new PartTimeVolatileLong();

            readIndexes.add(readIndex);
            readIndexHandlers.add(new ReadIndexHandler<>(handler, readIndex, exceptionHandler));
        }

        ReadIndexesTracker readIndexesTracker = new ReadIndexesTracker(readIndexes.toArray(new PartTimeVolatileLong[readIndexes.size()]));
        TetryonQueue<T> tetryonQueue = new TetryonQueue<>(capacity, newEventSupplier, eventOverwriter, readIndexesTracker, readParkTimeNanos);
        ReadIndexHandler<T>[] consumerHandlers = readIndexHandlers.toArray(new ReadIndexHandler[readIndexHandlers.size()]);
        return new TetryonComponents<>(consumerHandlers, readIndexes, tetryonQueue, readIndexesTracker, newEventSupplier, eventOverwriter);
    }
}
