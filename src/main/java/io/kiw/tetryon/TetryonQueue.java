package io.kiw.tetryon;

import io.kiw.tetryon.index.ReadIndexHandler;
import io.kiw.tetryon.index.ReadIndexesTracker;
import io.kiw.tetryon.publishermode.ReadIndex;
import io.kiw.tetryon.unsafe.PartTimeVolatileLong;

import static java.util.concurrent.locks.LockSupport.parkNanos;

public class TetryonQueue<T> {

    public static final int WRITE_PARK_TIME = 10;
    private final int limit;
    private final EventOverwriter<T> eventOverwriter;
    private final T[] instances;

    private final PartTimeVolatileLong writeIndex;

    private final ReadIndexesTracker readIndexesTracker;

    TetryonQueue(final int capacity, final NewEventSupplier<T> newEventSupplier, final EventOverwriter<T> eventOverwriter, ReadIndexesTracker readIndexesTracker, long readParkTimeNanos) {
        this.readIndexesTracker = readIndexesTracker;
        this.instances = buildNewEvents(capacity, newEventSupplier);
        this.limit = capacity - 1;
        this.eventOverwriter = eventOverwriter;
        writeIndex = new PartTimeVolatileLong();
    }

    boolean handleEvent(ReadIndexHandler<T> readIndexHandler) throws InterruptedException {
        final long currentReadIndex = readIndexHandler.getCurrentIndex();
        if(readIndexHandler.queueIsEmpty(currentReadIndex, writeIndex))
        {
           return false;
        }
        readIndexHandler.handleInstanceAndUpdateIndex(instances, limit, currentReadIndex);
        return true;
    }

    public void put(T t, long currentWriteIndex) {
        eventOverwriter.overwrite(instances[(int) currentWriteIndex & limit], t);
    }

    public boolean queueIsCompletelyEmpty(long currentWriteIndex, ReadIndex readIndex) {
        if( readIndex != null && readIndex.getMinReadIndex() != currentWriteIndex)
        {
            readIndex.setMinReadIndex(readIndexesTracker.getLowestReadIndexSafely());
        }
        else if (readIndex == null)
        {
            return readIndexesTracker.getLowestReadIndexSafely() == currentWriteIndex;
        }
        return readIndex.getMinReadIndex() == currentWriteIndex;
    }

    public void setWriteIndex(long newWriteIndex)
    {
        this.writeIndex.putOrderedLong(newWriteIndex);
    }

    public long getWriteIndex() {
        return writeIndex.getLong();
    }

    public long getVolatileWriteIndex() {
        return writeIndex.getVolatileLong();
    }

    public long getReadIndex() {
        return readIndexesTracker.getLowestReadIndexSafely();
    }

    private static <T> T[] buildNewEvents(int capacity, NewEventSupplier<T> instanceBuilder) {
        if((capacity & (capacity - 1)) != 0)
        {
            throw new RuntimeException("Capacity must be a factor of two");
        }
        Object[] objects = new Object[capacity];
        for (int i = 0; i < capacity; i++) {
            objects[i] = instanceBuilder.newInstance();
        }
        return (T[]) objects;
    }
}
