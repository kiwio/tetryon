package io.kiw.tetryon.unsafe;

import static io.kiw.tetryon.unsafe.UnsafeAccessor.UNSAFE;

public class PartTimeVolatileLong {

    private final long unsafeIndex;

    public PartTimeVolatileLong() {
        final long start = CacheLineHarness.getFullLineAddress();
        UNSAFE.setMemory(start, 8, (byte) 0);
        UNSAFE.putLong(start, 0);
        this.unsafeIndex = start;
    }

    public long getLong() {
        return UNSAFE.getLong(unsafeIndex);
    }

    public void putOrderedLong(long l) {
        UNSAFE.putOrderedLong(null, unsafeIndex, l);
    }

    public long getVolatileLong() {
        return UNSAFE.getLongVolatile(null, unsafeIndex);
    }
}
