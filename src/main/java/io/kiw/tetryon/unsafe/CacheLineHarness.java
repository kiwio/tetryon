package io.kiw.tetryon.unsafe;

import static io.kiw.tetryon.unsafe.UnsafeAccessor.UNSAFE;

public class CacheLineHarness {

    public static final int CACHE_LINE_SIZE = 64;

    public static long getFullLineAddress()
    {
        int attempts = 1024 * 128;
        long[] addressesToRelease = new long[attempts];

        for (int i = 0; i < attempts; i++) {
            long address = UNSAFE.allocateMemory(64);
            if(address % CACHE_LINE_SIZE == 0)
            {
                addressesToRelease[i] = address;
            }
            else
            {
                for (int releaseIndex = 0; releaseIndex < i; releaseIndex++) {
                    UNSAFE.freeMemory(addressesToRelease[releaseIndex]);
                }
                return address;
            }
        }

        throw new RuntimeException("Unable to get stable cache line");

    }
}
