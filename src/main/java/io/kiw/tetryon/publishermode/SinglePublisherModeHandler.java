package io.kiw.tetryon.publishermode;

import io.kiw.tetryon.*;
import io.kiw.tetryon.index.ReadIndexesTracker;

import static io.kiw.tetryon.TetryonQueue.WRITE_PARK_TIME;
import static java.util.concurrent.locks.LockSupport.parkNanos;

public class SinglePublisherModeHandler<T> implements PublisherModeHandler<T> {

    private final TetryonComponents<T> components;
    private final int capacity;
    private final SingleTetryonPublisherBatcher<T> batcher;

    private final ReadIndex minReadIndexCache = new ReadIndex();

    public SinglePublisherModeHandler(TetryonComponents<T> components, int capacity, T[] batchedItems) {
        this.components = components;
        this.capacity = capacity;
        this.batcher = new SingleTetryonPublisherBatcher<>(this, batchedItems, components.eventOverwriter);
    }

    @Override
    public void publishEvent(T t, int publisherThreadIndex) {

        final long currentWriteIndex = components.tetryonQueue.getWriteIndex();
        while(queueIsFull(currentWriteIndex))
        {
            parkNanos(WRITE_PARK_TIME);
        }
        components.tetryonQueue.put(t, currentWriteIndex);
        components.tetryonQueue.setWriteIndex(currentWriteIndex + 1);

    }

    @Override
    public TetryonPublisherBatcher<T> getBatcher() {
        return batcher;
    }

    @Override
    public void flushBatch(T[] batchedItems, int batchSize) {
        final long currentWriteIndex = components.tetryonQueue.getWriteIndex();
        while(queueCanNotHandleUpTo(currentWriteIndex + batchSize))
        {
            parkNanos(WRITE_PARK_TIME);
        }

        for (int i = 0; i < batchSize; i++) {
            components.tetryonQueue.put(batchedItems[i], currentWriteIndex + i);

        }

        components.tetryonQueue.setWriteIndex(currentWriteIndex + batchSize);
    }

    @Override
    public TetryonComponents<T>[] getTetryonComponents() {
        return new TetryonComponents[]{components};
    }

    private boolean queueCanNotHandleUpTo(long writeIndexToCompleteBatch) {
        long minReadIndex = writeIndexToCompleteBatch - capacity;
        if( minReadIndexCache.getMinReadIndex() <= minReadIndex)
        {
            minReadIndexCache.setMinReadIndex(components.readIndexesTracker.getLowestReadIndexSafely());
            return minReadIndexCache.getMinReadIndex() <= minReadIndex;
        }
        return false;
    }

    private boolean queueIsFull(long currentWriteIndex) {
        long minReadIndex = currentWriteIndex - capacity;
        if( minReadIndexCache.getMinReadIndex() == minReadIndex)
        {
            minReadIndexCache.setMinReadIndex(components.readIndexesTracker.getLowestReadIndexSafely());
            return minReadIndexCache.getMinReadIndex() == minReadIndex;
        }
        return false;
    }

    @Override
    public void await() {
        long currentWriteIndex = components.tetryonQueue.getWriteIndex();

        while(!components.tetryonQueue.queueIsCompletelyEmpty(currentWriteIndex, minReadIndexCache))
        {
            parkNanos(WRITE_PARK_TIME);
        }
    }
}
