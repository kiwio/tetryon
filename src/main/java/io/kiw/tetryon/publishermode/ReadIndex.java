package io.kiw.tetryon.publishermode;

public class ReadIndex {
    private long minReadIndex = 0;

    public long getMinReadIndex() {
        return minReadIndex;
    }

    public void setMinReadIndex(long minReadIndex) {
        this.minReadIndex = minReadIndex;
    }
}
