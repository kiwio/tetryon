package io.kiw.tetryon.publishermode;

import io.kiw.tetryon.TetryonComponents;
import io.kiw.tetryon.TetryonPublisherBatcher;

import java.util.concurrent.ConcurrentMap;

public class LazyMultiPublisherModeHandler<T> implements PublisherModeHandler<T> {
    private final TetryonComponents<T> components;
    private final SharedMultiPublisher<T> sharedMultiPublisher;
    private final ConcurrentMap<Long, ReadIndex> threadToIndexMap;

    public LazyMultiPublisherModeHandler(TetryonComponents<T> components, SharedMultiPublisher<T> sharedMultiPublisher, ConcurrentMap<Long, ReadIndex> threadToIndexMap) {
        this.components = components;
        this.sharedMultiPublisher = sharedMultiPublisher;
        this.threadToIndexMap = threadToIndexMap;
    }

    @Override
    public  void publishEvent(T t, int publisherThreadIndex) {
        long threadId = Thread.currentThread().getId();
        if(!threadToIndexMap.containsKey(threadId))
        {
            threadToIndexMap.putIfAbsent(threadId, new ReadIndex());
        }
        sharedMultiPublisher.put(t, threadToIndexMap.get(threadId));
    }

    @Override
    public  void await() {
        sharedMultiPublisher.await(threadToIndexMap.get(Thread.currentThread().getId()));
    }

    @Override
    public TetryonPublisherBatcher<T> getBatcher() {
        throw new UnsupportedOperationException("Batcher not supported for multi publisher implementation");
    }

    @Override
    public void flushBatch(T[] batchedItems, int batchIndex) {
        throw new UnsupportedOperationException("Batcher not supported for multi publisher implementation");
    }

    @Override
    public TetryonComponents<T>[] getTetryonComponents() {
        return new TetryonComponents[]{components};
    }
}
