package io.kiw.tetryon.publishermode;

import io.kiw.tetryon.TetryonComponents;
import io.kiw.tetryon.TetryonPublisherBatcher;

public class MultiPublisherModeHandler<T> implements PublisherModeHandler<T> {

    private final PublisherModeHandler<T>[] singlePublisherPerThread;

    public MultiPublisherModeHandler(PublisherModeHandler<T>[] singlePublisherPerThread) {
        this.singlePublisherPerThread = singlePublisherPerThread;
    }

    @Override
    public void publishEvent(T t, int publisherThreadIndex) {
        singlePublisherPerThread[publisherThreadIndex].publishEvent(t, publisherThreadIndex);
    }

    @Override
    public void await() {
        for (int i = 0; i < singlePublisherPerThread.length; i++) {
            singlePublisherPerThread[i].await();
        }
    }

    @Override
    public TetryonPublisherBatcher<T> getBatcher() {
        throw new UnsupportedOperationException("Batcher not supported for multi publisher implementation");
    }

    @Override
    public void flushBatch(T[] batchedItems, int batchIndex) {
        throw new UnsupportedOperationException("Batcher not supported for multi publisher implementation");
    }

    @Override
    public TetryonComponents<T>[] getTetryonComponents() {
        TetryonComponents<T>[] tetryonComponents = new TetryonComponents[singlePublisherPerThread.length];
        for (int i = 0; i < singlePublisherPerThread.length; i++) {
            tetryonComponents[i] = singlePublisherPerThread[i].getTetryonComponents()[0];
        }

        return tetryonComponents;
    }

}
