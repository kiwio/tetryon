package io.kiw.tetryon.publishermode;

import io.kiw.tetryon.TetryonQueue;

import java.util.concurrent.atomic.AtomicLong;

import static io.kiw.tetryon.TetryonQueue.WRITE_PARK_TIME;
import static java.util.concurrent.locks.LockSupport.parkNanos;

public class SharedMultiPublisher<T> {
    private TetryonQueue<T> tetryonQueue;
    private int capacity;
    private final AtomicLong multiPublisherWriteIndex = new AtomicLong(0);

    public SharedMultiPublisher(TetryonQueue<T> tetryonQueue, int capacity) {

        this.tetryonQueue = tetryonQueue;
        this.capacity = capacity;
    }

    public void put(T t, ReadIndex readIndex) {
        final long multiPublisherWriteIndexValue;

        multiPublisherWriteIndexValue = this.multiPublisherWriteIndex.getAndIncrement();

        while (queueIsFull(multiPublisherWriteIndexValue, readIndex)) {
            parkNanos(WRITE_PARK_TIME);
        }

        tetryonQueue.put(t, multiPublisherWriteIndexValue);
        long currentWriteIndex;
        do
        {
            currentWriteIndex = tetryonQueue.getVolatileWriteIndex();
        }
        while(multiPublisherWriteIndexValue != currentWriteIndex);

        tetryonQueue.setWriteIndex(currentWriteIndex + 1);
    }

    private boolean queueIsFull(long currentWriteIndex, ReadIndex publisherReadIndex) {
        long lowestPossibleReadIndex = currentWriteIndex - capacity;
        if(publisherReadIndex.getMinReadIndex() <= lowestPossibleReadIndex)
        {
            long realReadIndex = tetryonQueue.getReadIndex();

            publisherReadIndex.setMinReadIndex(realReadIndex);
            return publisherReadIndex.getMinReadIndex() <= realReadIndex;
        }
        return false;
    }

    public void await(ReadIndex readIndex) {
        long currentWriteIndex = tetryonQueue.getVolatileWriteIndex();
        long currentMultiPublisherWriteIndex = multiPublisherWriteIndex.get();

        while(currentWriteIndex != currentMultiPublisherWriteIndex || !tetryonQueue.queueIsCompletelyEmpty(currentWriteIndex, readIndex))
        {
            currentWriteIndex = tetryonQueue.getVolatileWriteIndex();
            currentMultiPublisherWriteIndex = multiPublisherWriteIndex.get();
            parkNanos(WRITE_PARK_TIME);
        }
    }
}
