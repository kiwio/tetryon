package io.kiw.tetryon.publishermode;

import io.kiw.tetryon.TetryonComponents;
import io.kiw.tetryon.TetryonPublisherBatcher;

public interface PublisherModeHandler<T> {
    void publishEvent(T t, int publisherThreadIndex);

    void await();

    TetryonPublisherBatcher<T> getBatcher();

    void flushBatch(T[] batchedItems, int batchIndex);

    TetryonComponents<T>[] getTetryonComponents();
}
