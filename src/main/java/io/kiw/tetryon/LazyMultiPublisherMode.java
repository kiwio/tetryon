package io.kiw.tetryon;

import io.kiw.tetryon.publishermode.LazyMultiPublisherModeHandler;
import io.kiw.tetryon.publishermode.PublisherModeHandler;
import io.kiw.tetryon.publishermode.ReadIndex;
import io.kiw.tetryon.publishermode.SharedMultiPublisher;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class LazyMultiPublisherMode<T> implements PublisherMode<T> {

    private final ConcurrentMap<Long, ReadIndex> threadToIndexMap = new ConcurrentHashMap<>();

    public LazyMultiPublisherMode() {
    }



    @Override
    public void start()
    {
    }

    @Override
    public PublisherModeHandler<T> buildPublisherModeHandler(List<Tetryon.EventHandler<T>> eventHandlers, EventOverwriter<T> eventOverwriter, long readParkTimeNanos, int capacity, NewEventSupplier<T> newEventSupplier, ExceptionHandler exceptionHandler) {

        TetryonComponents<T> components = TetryonComponents.build(eventHandlers, eventOverwriter, readParkTimeNanos, capacity, newEventSupplier, exceptionHandler);
        return new LazyMultiPublisherModeHandler<>(components, new SharedMultiPublisher<>(components.tetryonQueue, capacity), threadToIndexMap);
    }
}
