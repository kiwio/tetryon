package io.kiw.tetryon;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class EventHandlerGroup {
    public static <T> List<Tetryon.EventHandler<T>> createGroup(Supplier<Tetryon.EventHandler<T>> newEventHandlerSupplier, int threadCount) {
        List<Tetryon.EventHandler<T>> eventHandlers = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            eventHandlers.add(new PartTimeEventHandler<>(newEventHandlerSupplier.get(), i, threadCount));
        }
        return eventHandlers;
    }

    private static class PartTimeEventHandler<T> implements Tetryon.EventHandler<T>{
        private final Tetryon.EventHandler<T> eventHandler;
        private final int threadCount;
        private int startingIndex;

        public PartTimeEventHandler(Tetryon.EventHandler<T> eventHandler, int startingIndex, int threadCount) {

            this.eventHandler = eventHandler;
            this.startingIndex = startingIndex;
            this.threadCount = threadCount;
        }

        @Override
        public void handle(T event) {
            if(startingIndex++ % threadCount == 0)
            {
                this.eventHandler.handle(event);
            }
        }
    }
}
