package io.kiw.tetryon;

public interface EventOverwriter<T> {
    T overwrite(final T instanceToReplace, final T replacementInstance);
}
