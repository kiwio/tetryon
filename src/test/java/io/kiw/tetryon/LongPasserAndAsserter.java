package io.kiw.tetryon;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class LongPasserAndAsserter implements Passer {
    private final List<AssertionError> errorList;
    private long expectedLong = 0;

    public LongPasserAndAsserter(List<AssertionError> errorList) {

        this.errorList = errorList;
    }

    @Override
    public void pass(LongEvent actual) {
        try
        {
            assertEquals(expectedLong, actual.value);
        }
        catch (AssertionError e)
        {
            errorList.add(e);
        }
        expectedLong++;
    }

}
