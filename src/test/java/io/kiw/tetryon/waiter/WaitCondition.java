package io.kiw.tetryon.waiter;

public interface WaitCondition {
    boolean conditionIsMet();
}
