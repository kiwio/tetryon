package io.kiw.tetryon.waiter;

public class Waiter {

    private static final int RETRY_INTERVAL = 1000;
    private static final int TIMEOUT = 10000;

    public static void waitFor(final WaitCondition waitCondition) {
        final long startTime = System.currentTimeMillis();
        Throwable caughtException = null;
        while(true)
        {
            try {
                if (waitCondition.conditionIsMet()) {
                    return;
                }
            }
            catch (Throwable ignored)
            {
                caughtException = ignored;
            }

            try {
                Thread.sleep(RETRY_INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(System.currentTimeMillis() > startTime + TIMEOUT)
            {
                break;
            }
        }

        if(caughtException != null)
        {
            throw new AssertionError(caughtException);
        }
        else
        {
            throw new AssertionError("Failed to meet condition in time");
        }
    }
}
