package io.kiw.tetryon;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.*;

public class TetryonTestHarness {
    private final List<AssertionError> assertionErrors = Collections.synchronizedList(new ArrayList<>());
    private final Map<Long, Long> expectedStreamResults = new ConcurrentHashMap<>();
    private final Set<Long> openPublisherStreams = ConcurrentHashMap.newKeySet();
    private Set<Thread> publisherThreads = new HashSet<>();
    private final List<Throwable> caughtExceptions = Collections.synchronizedList(new ArrayList<>());


    public MultiPubLongWrapper newEvent() {
        return new MultiPubLongWrapper();
    }

    public MultiPubLongWrapper overwrite(MultiPubLongWrapper t, MultiPubLongWrapper t1) {
        t.overwrite(t1);
        return t;
    }

    public Tetryon.EventHandler<MultiPubLongWrapper> getSubscriber(int subscriberStream)
    {
        return event -> {
            long key = deriveStreamKey(event.streamValue, subscriberStream);

            try {
                assertTrue("Attempted to consume for publisher " + event.streamValue + " and subscriber " + subscriberStream + " but was already doing so on another thread", !openPublisherStreams.contains(key));

                openPublisherStreams.add(key);
                expectedStreamResults.putIfAbsent(key, 0L);


                long expected = expectedStreamResults.get(key);
                assertEquals("Unexpected sequence number on publisher " + event.streamValue + " and subscriber " + subscriberStream, expected, event.longValue);
            }
            catch (AssertionError e)
            {
                assertionErrors.add(e);
            }
            finally {
                openPublisherStreams.remove(key);
                expectedStreamResults.put(key, event.longValue + 1);
            }
        };
    }

    public void assertNoErrorsAndStreamsAtSizeOf(int iterations) {
        if(!assertionErrors.isEmpty())
        {
            throw assertionErrors.get(0);
        }


        if(expectedStreamResults.size() == 0)
        {
            fail("No results were received, did you forget to start tetryon?");
        }
        for (Long actual : expectedStreamResults.values()) {
            assertEquals(iterations, actual.longValue());
        }

    }

    public void createStreamInNewPublisherThread(Tetryon<MultiPubLongWrapper> tetryon, int iterations, int streamValue) {
        Thread thread = new Thread(() -> {
            final MultiPubLongWrapper writerInstance = newEvent();

            for (int i = 0; i < iterations; i++) {
                tetryon.publishEvent(writerInstance.overwrite(streamValue, i), streamValue - 1);
            }
        });

        publisherThreads.add(thread);
    }

    public void startAndWaitForPublisherStreams() {

        for (Thread publisherThread : publisherThreads) {
            publisherThread.start();
        }
        for (Thread publisherThread : publisherThreads) {
            try {
                publisherThread.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public Set<Thread> getPublisherThreads() {
        return publisherThreads;
    }

    public void addException(Throwable throwable) {
        this.caughtExceptions.add(throwable);
    }

    public static class MultiPubLongWrapper {
        int streamValue = 0;
        long longValue;

        MultiPubLongWrapper overwrite(MultiPubLongWrapper replacementInstance) {
            this.streamValue = replacementInstance.streamValue;
            this.longValue = replacementInstance.longValue;
            return this;
        }

        MultiPubLongWrapper overwrite(int streamValue, long l) {
            this.streamValue = streamValue;
            this.longValue = l;
            return this;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            MultiPubLongWrapper that = (MultiPubLongWrapper) o;

            if (streamValue != that.streamValue) return false;
            return longValue == that.longValue;
        }

        @Override
        public int hashCode() {
            int result = streamValue;
            result = 31 * result + (int) (longValue ^ (longValue >>> 32));
            return result;
        }

        @Override
        public String toString() {
            return "MultiPubLongWrapper{" +
                    "streamValue=" + streamValue +
                    ", longValue=" + longValue +
                    '}';
        }
    }

    private long deriveStreamKey(int publisherStreamId, int subscriberStreamId)
    {
        return ((long)publisherStreamId << 32) + subscriberStreamId;
    }

    public void assertNoUncaughtExceptions() {
        if(!caughtExceptions.isEmpty())
        {
            throw new AssertionError("found exceptions " + caughtExceptions);
        }
    }

}
