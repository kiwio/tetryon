package io.kiw.tetryon;

public class LongEvent {
    long value = 0;

    public LongEvent overwrite(LongEvent replacementInstance) {
        value = replacementInstance.value;
        return this;
    }
}
