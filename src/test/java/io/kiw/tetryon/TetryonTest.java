package io.kiw.tetryon;

import org.junit.After;
import org.junit.Test;

import java.util.Arrays;

public class TetryonTest {

    private static final int CAPACITY = 512;
    private Tetryon toShutdown;
    @Test
    public void shouldSingleProducerSingleConsumer() throws Exception {
        TetryonTestHarness tetryonTestHarness = new TetryonTestHarness();
        Tetryon<TetryonTestHarness.MultiPubLongWrapper> tetryon = Tetryon.buildInstance(CAPACITY,
                TetryonTestHarness.MultiPubLongWrapper::new,
                TetryonTestHarness.MultiPubLongWrapper::overwrite,
                new SinglePublisherMode<>(),
                Arrays.asList(tetryonTestHarness.getSubscriber(1)), 100, tetryonTestHarness::addException, new TetryonThreadFactory());
        toShutdown = tetryon;

        tetryon.start();

        int iterations = 1000000;
        TetryonTestHarness.MultiPubLongWrapper writerInstance = tetryonTestHarness.newEvent();
        for (long i = 0; i < iterations; i++) {
            tetryon.publishEvent(writerInstance.overwrite(1, i));
        }

        tetryon.await();
        tetryonTestHarness.assertNoErrorsAndStreamsAtSizeOf(iterations);
        tetryonTestHarness.assertNoUncaughtExceptions();
    }



    @Test
    public void shouldSupportMultiplePublisherAndMultipleconsumers() throws Exception {
        TetryonTestHarness tetryonTestHarness = new TetryonTestHarness();
        MultiPublisherMode<TetryonTestHarness.MultiPubLongWrapper> publisherMode = new MultiPublisherMode<>(4);
        Tetryon<TetryonTestHarness.MultiPubLongWrapper> tetryon =
                Tetryon.buildInstance(CAPACITY,
                        tetryonTestHarness::newEvent,
                        tetryonTestHarness::overwrite,
                        publisherMode,
                        Arrays.asList(tetryonTestHarness.getSubscriber(1), tetryonTestHarness.getSubscriber(2), tetryonTestHarness.getSubscriber(3), tetryonTestHarness.getSubscriber(4)),
                        100, tetryonTestHarness::addException, new TetryonThreadFactory());
        final int iterations = CAPACITY * 100;
        toShutdown = tetryon;

        tetryonTestHarness.createStreamInNewPublisherThread(tetryon, iterations, 1);
        tetryonTestHarness.createStreamInNewPublisherThread(tetryon, iterations, 2);
        tetryonTestHarness.createStreamInNewPublisherThread(tetryon, iterations, 3);
        tetryonTestHarness.createStreamInNewPublisherThread(tetryon, iterations, 4);

        tetryon.start();

        tetryonTestHarness.startAndWaitForPublisherStreams();
        tetryon.await();
        tetryonTestHarness.assertNoErrorsAndStreamsAtSizeOf(iterations);
        tetryonTestHarness.assertNoUncaughtExceptions();

    }

    @Test
    public void shouldSupportLazyMultiplePublishers() throws Exception {
        TetryonTestHarness tetryonTestHarness = new TetryonTestHarness();
        Tetryon<TetryonTestHarness.MultiPubLongWrapper> tetryon = Tetryon.buildInstance(CAPACITY,
                TetryonTestHarness.MultiPubLongWrapper::new,
                TetryonTestHarness.MultiPubLongWrapper::overwrite,
                new LazyMultiPublisherMode<>(),
                Arrays.asList(tetryonTestHarness.getSubscriber(1)), 100, tetryonTestHarness::addException, new TetryonThreadFactory());

        toShutdown = tetryon;

        tetryon.start();

        final int iterations = 1000000;


        tetryonTestHarness.createStreamInNewPublisherThread(tetryon, iterations, 1);
        tetryonTestHarness.createStreamInNewPublisherThread(tetryon, iterations, 2);

        tetryonTestHarness.startAndWaitForPublisherStreams();
        tetryon.await();
        tetryonTestHarness.assertNoErrorsAndStreamsAtSizeOf(iterations);
        tetryonTestHarness.assertNoUncaughtExceptions();

    }

    @Test
    public void shouldCopeWithMultipleConsumers() throws Exception {
        TetryonTestHarness tetryonTestHarness = new TetryonTestHarness();
        Tetryon<TetryonTestHarness.MultiPubLongWrapper> tetryon = setUpMultiConsumerInstance(tetryonTestHarness);
        toShutdown = tetryon;
        tetryon.start();
        final int iterations = 1000000;
        TetryonTestHarness.MultiPubLongWrapper multiPubLongWrapper = tetryonTestHarness.newEvent();
        for (long i = 0; i < iterations; i++) {
            tetryon.publishEvent(multiPubLongWrapper.overwrite(1, i));
        }

        tetryon.await();

        tetryonTestHarness.assertNoErrorsAndStreamsAtSizeOf(iterations);
        tetryonTestHarness.assertNoUncaughtExceptions();

    }

    @Test
    public void shouldPublishByBatching() throws Exception {
        TetryonTestHarness tetryonTestHarness = new TetryonTestHarness();
        Tetryon<TetryonTestHarness.MultiPubLongWrapper> tetryon = Tetryon.buildInstance(CAPACITY,
                TetryonTestHarness.MultiPubLongWrapper::new,
                TetryonTestHarness.MultiPubLongWrapper::overwrite,
                new SinglePublisherMode<>(),
                Arrays.asList(tetryonTestHarness.getSubscriber(1)), 100, tetryonTestHarness::addException, new TetryonThreadFactory());


        tetryon.start();
        toShutdown = tetryon;
        TetryonPublisherBatcher<TetryonTestHarness.MultiPubLongWrapper> batcher = tetryon.getBatcher();
        int iterations = 1000000;
        TetryonTestHarness.MultiPubLongWrapper writerInstance = tetryonTestHarness.newEvent();
        int batchSize = 10;
        for (long i = 0; i < iterations / batchSize; i++) {
            batcher.startBatch();
            for (int j = 0; j < batchSize; j++) {
                batcher.publishEvent(writerInstance.overwrite(1, (i * batchSize) + j));
            }
            batcher.flushBatch();

        }

        tetryon.await();
        tetryonTestHarness.assertNoErrorsAndStreamsAtSizeOf(iterations);
        tetryonTestHarness.assertNoUncaughtExceptions();

    }

    @Test
    public void shouldCopeWithMultipleProducers() throws Exception {
        TetryonTestHarness tetryonTestHarness = new TetryonTestHarness();
        MultiPublisherMode<TetryonTestHarness.MultiPubLongWrapper> publisherMode = new MultiPublisherMode<>(2);
        Tetryon<TetryonTestHarness.MultiPubLongWrapper> tetryon =
                Tetryon.buildInstance(
                        CAPACITY,
                        tetryonTestHarness::newEvent,
                        tetryonTestHarness::overwrite,
                        publisherMode,
                        Arrays.asList(
                                tetryonTestHarness.getSubscriber(1)
                        ),
                        100,
                        tetryonTestHarness::addException, new TetryonThreadFactory());
        final int iterations = CAPACITY * 10000;
        toShutdown = tetryon;

        tetryonTestHarness.createStreamInNewPublisherThread(tetryon, iterations, 1);
        tetryonTestHarness.createStreamInNewPublisherThread(tetryon, iterations, 2);

        tetryon.start();

        tetryonTestHarness.startAndWaitForPublisherStreams();
        tetryon.await();
        tetryonTestHarness.assertNoErrorsAndStreamsAtSizeOf(iterations);
        tetryonTestHarness.assertNoUncaughtExceptions();

    }

    @Test
    public void shouldFillBufferWithMultiplePublishersThenDrain() throws Exception {
        TetryonTestHarness tetryonTestHarness = new TetryonTestHarness();
        int capacity = 512 * 128;
        MultiPublisherMode<TetryonTestHarness.MultiPubLongWrapper> publisherMode = new MultiPublisherMode<>(4);
        Tetryon<TetryonTestHarness.MultiPubLongWrapper> tetryon = Tetryon.buildInstance(capacity, tetryonTestHarness::newEvent, tetryonTestHarness::overwrite, publisherMode,
                Arrays.asList(tetryonTestHarness.getSubscriber(1)), 100, tetryonTestHarness::addException, new TetryonThreadFactory());
        toShutdown = tetryon;
        final int iterations = capacity / 4;
        tetryonTestHarness.createStreamInNewPublisherThread(tetryon, iterations, 1);
        tetryonTestHarness.createStreamInNewPublisherThread(tetryon, iterations, 2);
        tetryonTestHarness.createStreamInNewPublisherThread(tetryon, iterations, 3);
        tetryonTestHarness.createStreamInNewPublisherThread(tetryon, iterations, 4);

        tetryonTestHarness.startAndWaitForPublisherStreams();
        tetryon.start();

        tetryon.await();
        tetryonTestHarness.assertNoErrorsAndStreamsAtSizeOf(iterations);
        tetryonTestHarness.assertNoUncaughtExceptions();
    }

    private Tetryon<TetryonTestHarness.MultiPubLongWrapper> setUpMultiConsumerInstance(TetryonTestHarness tetryonTestHarness) {
        return Tetryon.buildInstance(CAPACITY,
                TetryonTestHarness.MultiPubLongWrapper::new,
                TetryonTestHarness.MultiPubLongWrapper::overwrite,
                new SinglePublisherMode<>(),
                Arrays.asList(tetryonTestHarness.getSubscriber(1), tetryonTestHarness.getSubscriber(2), tetryonTestHarness.getSubscriber(3), tetryonTestHarness.getSubscriber(4)), 100, tetryonTestHarness::addException, new TetryonThreadFactory());
    }


    @After
    public void tearDown() throws Exception {
        toShutdown.shutdown();
    }
}